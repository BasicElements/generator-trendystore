# generator-trendystore

[![Build Status](https://img.shields.io/travis/BasicElements/generator-trendystore.svg)](https://travis-ci.org/BasicElements/generator-trendystore)
[![Dependency Status](https://img.shields.io/david/basicelements/generator-trendystore.svg)](https://david-dm.org/basicelements/generator-trendystore)
[![NPM version](http://img.shields.io/npm/v/generator-trendystore.svg)](http://npmjs.org/generator-trendystore)
[![NPM downloads](http://img.shields.io/npm/dm/generator-trendystore.svg)](http://npmjs.org/generator-trendystore)

> [Yeoman](http://yeoman.io) generator

## Getting Started

### What is Yeoman?

Trick question. It's not a thing. It's this guy:

![](http://i.imgur.com/JHaAlBJ.png)

Basically, he wears a top hat, lives in your computer, and waits for you to tell him what kind of application you wish to create.

Not every new computer comes with a Yeoman pre-installed. He lives in the [npm](https://npmjs.org) package repository. You only have to ask for him once, then he packs up and moves into your hard drive. *Make sure you clean up, he likes new and shiny things.*

### Install Yeoman

```bash
npm install -g yo
```

### Install the generator.

To install generator-trendystore from npm, run:

```bash
npm install -g generator-trendystore
```

### Install [grunt-cli](https://www.npmjs.com/package/grunt-cli).

To install grunt-cli from npm, run:

```bash
npm install -g grunt-cli
```

Now you'll have access to the grunt command anywhere on your system.

## Create your project.

```bash
mkdir projectName && cd projectName
```

## Finally, initiate the generator:

```bash
yo trendystore
```

## Start developing!

Use [Grunt](https://gruntjs.com) to start a server.

```bash
grunt serve
```

When finished run:

```bash
grunt
```

## License

MIT
