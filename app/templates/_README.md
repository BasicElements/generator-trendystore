## <%= storeName %>

[![Dependency Status](https://img.shields.io/david/<%= githubUser %>/<%= storeName %>.svg)](https://david-dm.org/<%= githubUser %>/<%= storeName %>)
### Install

```bash
git clone https://github.com/<%= githubUser %>/<%= storeName %>
```

### Development

```bash
cd <%= storeName %> && grunt serve
```

#### Some notes

> the store will be available [here](http://localhost:9000/)

> the admin side of trendystore-demo will be available [here](http://localhost:9000/#/admin)

### Issues

[Report Issue](https://github.com/<%= githubUser %>/<%= storeName %>).

### License

<%= license %>
