(function (document) {
  'use strict';
  var app = document.querySelector('#app');

  document.addEventListener('polymer-ready', function () {
    console.log('Ready, Set, Go!');
  });

  function store() {
    app.route = '<%= storeName %>-store';
  }

  function admin() {
    app.route = '<%= storeName %>-admin';
  }

  function config() {
    app.route = '<%= storeName %>-config';
  }
  // Routing
  // Routes
  page('/', store);
  page('/admin', admin);
  page('/config', config);

  // Configuration
  page({hashbang: true});

  // TODO: Add Analystics

// wrap document so it plays nice with other libraries
// http://www.polymer-project.org/platform/shadow-dom.html#wrappers
})(wrap(document));
