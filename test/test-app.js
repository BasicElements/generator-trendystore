'use strict';

var path = require('path');
var assert = require('yeoman-generator').assert;
var helpers = require('yeoman-generator').test;
var os = require('os');

describe('trendystore:app', function () {
  before(function (done) {
    helpers.run(path.join(__dirname, '../app'))
      .inDir(path.join(os.tmpdir(), './temp-test'))
      .withArguments(['--skip-install'])
      .withPrompt({
        githubUser: "someValue",
        storeName: "trendystore",
        license: "MIT",
        lang: "en",
        includeSass: true,
        imgurClientId: 'default'
      })
      .on('end', done);
  });

  it('creates files', function () {
    assert.file([
      'bower.json',
      'package.json',
      '.bowerrc',
      '.gitignore',
      '.editorconfig',
      '.jshintrc',
      'Gruntfile.js',
      'app/index.html',
      'app/manifest.json',
      // 'app/manifest.webapp',
      'app/styles/main.scss',
      'app/scripts/app.js',
      'app/robots.txt',
      'app/elements/elements.html',
      'app/elements/trendystore-store/trendystore-store.html',
      'app/elements/trendystore-store/trendystore-store.scss',
      'app/elements/trendystore-admin/trendystore-admin.html',
      'app/elements/trendystore-admin/trendystore-admin.scss'
    ]);
  });
});
